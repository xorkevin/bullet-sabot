# TODO

## REST api

- [x] routing for api calls
  - [ ] forum
  - [x] pages
  - [ ] dashboard
    - [ ] should modify configuration
  - [ ] blog
- [ ] socketio, haproxy
- [x] gunicorn custom application
  - [x] gunicorn worker class
  - [ ] make gunicorn import workers conditionally
- [ ] authentication
  - [ ] need conditional authentication for certain routes

### Done
- [x] create test script
- [x] refactor api to falcon from flask
