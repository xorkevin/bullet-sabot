#!/usr/bin/env bash

docker kill bullet-sabot-container-1 && docker rm bullet-sabot-container-1
docker kill bullet-sabot-container-2 && docker rm bullet-sabot-container-2
docker kill bullet-sabot-container-3 && docker rm bullet-sabot-container-3
