from .gcdservice import GCDService
from .mockservice import MockService

class DataServices:
    GCDService = GCDService
    MockService = MockService
